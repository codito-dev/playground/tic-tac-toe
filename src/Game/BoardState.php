<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

enum BoardState: string
{
    case Open = 'open';
    case Draw = 'draw';
    case Win = 'win';
}
