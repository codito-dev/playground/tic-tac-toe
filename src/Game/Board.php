<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

use Codito\Kata\TicTacToe\Game\Side\Marker;

/**
 * @phpstan-type MarkersArrangement array<int, array<int, Marker>>
 */
final class Board
{
    /** @phpstan-var MarkersArrangement */
    private array $markers = [];
    private int $markersPlaced = 0;
    private BoardState $state = BoardState::Open;
    private ?Marker $winningMarker = null;
    private ?WinningSetType $winningSetType = null;
    private ?int $winningSetIndex = null;

    public function __construct(private readonly int $size) {
        if ($size < 3) {
            throw new \InvalidArgumentException('Board can\'t be less than 3x3');
        }

        for ($i = 1; $i <= $size; $i++) {
            $this->markers[$i] = array_fill(1, $size, Marker::Empty);
        }
    }

    public function placeMarker(Marker $marker, int $row, int $column): void
    {
        if (!isset($this->markers[$row][$column])) {
            throw new \InvalidArgumentException('This move is invalid');
        }

        if (Marker::Empty !== $this->markers[$row][$column]) {
            throw new \InvalidArgumentException('This field is already taken');
        }

        $this->markers[$row][$column] = $marker;
        $this->markersPlaced++;

        $this->resolveState();
    }

    public function state(): BoardState
    {
        return $this->state;
    }

    public function winningMarker(): ?Marker
    {
        return $this->winningMarker;
    }

    public function winningSetType(): ?WinningSetType
    {
        return $this->winningSetType;
    }

    public function winningSetIndex(): ?int
    {
        return $this->winningSetIndex;
    }

    private function resolveState(): void
    {
        if ($this->checkRows() || $this->checkColumns() || $this->checkDiagonals()) {
            $this->state = BoardState::Win;
        }

        if ($this->markersPlaced === pow($this->size, 2)) {
            $this->state = BoardState::Draw;
        }
    }

    private function checkRows(): bool
    {
        foreach ($this->markers as $row => $markers) {
            if ($this->checkSet($markers)) {
                $this->winningSetType = WinningSetType::Row;
                $this->winningSetIndex = $row;
                $this->winningMarker = $markers[1];

                return true;
            }
        }

        return false;
    }

    private function checkColumns(): bool
    {
        for ($column = 1; $column <= $this->size; $column++) {
            if ($this->checkSet(array_column($this->markers, $column))) {
                $this->winningSetType = WinningSetType::Column;
                $this->winningSetIndex = $column;
                $this->winningMarker = $this->markers[1][$column];

                return true;
            }
        }

        return false;
    }

    private function checkDiagonals(): bool
    {
        $setsToCheck = [];

        for ($i = 1; $i <= $this->size; $i++) {
            $setsToCheck['slope'][] = $this->markers[$i][$i];
            $setsToCheck['steep'][] = $this->markers[$this->size + 1 - $i][$i];
        }

        foreach ($setsToCheck as $type => $set) {
            if ($this->checkSet($set)) {
                $this->winningSetType = $type === 'slope'
                    ? WinningSetType::DiagonalSlope
                    : WinningSetType::DiagonalSteep;
                $this->winningMarker = $type === 'slope'
                    ? $this->markers[1][1]
                    : $this->markers[$this->size][1];

                return true;
            }
        }

        return false;
    }

    /**
     * @param array<int, Marker> $markers
     */
    private function checkSet(array $markers): bool
    {
        $uniqueMarkers = array_unique(array_map(fn (Marker $marker) => $marker->value, $markers));

        return 1 === count($uniqueMarkers) && Marker::Empty !== current($markers);
    }
}
