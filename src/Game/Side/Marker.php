<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game\Side;

enum Marker: string
{
    case Empty = '';
    case Nought = 'O';
    case Cross = 'X';
}
