<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game\Side;

final readonly class Sides
{
    private Side $noughts;
    private Side $crosses;

    /**
     * @param array{noughts: array<Player>, crosses: array<Player>} $players
     */
    private function __construct(array $players)
    {
        $this->validateSide($players['noughts']);
        $this->validateSide($players['crosses']);
        $this->validatePlayers(array_merge($players['noughts'], $players['crosses']));

        $this->noughts = new Side(Marker::Nought, ...$players['noughts']);
        $this->crosses = new Side(Marker::Cross, ...$players['crosses']);
    }

    public function side(Marker $marker): Side
    {
        return match($marker) {
            Marker::Nought => $this->noughts,
            Marker::Cross => $this->crosses,
            Marker::Empty => throw new \RuntimeException('Can\'t pick an empty side'),
        };
    }

    public static function createByAlternatingAssignment(Player ...$players): self
    {
        $sides = ['crosses' => [], 'noughts' => []];

        foreach ($players as $i => $player) {
            $sides[(0 === $i % 2) ? 'crosses' : 'noughts'][] = $player;
        }

        return new self($sides);
    }

    /**
     * @param array<Player> $players
     */
    private function validateSide(array $players): void
    {
        if (1 !== count($players)) {
            throw new \InvalidArgumentException('Unsupported amount of players');
        }
    }

    /**
     * @param array<Player> $players
     */
    private function validatePlayers(array $players): void
    {
        $names = array_unique(array_map(fn (Player $player) => $player->name, $players));

        if (count($names) !== count($players)) {
            throw new \InvalidArgumentException('Players\' names must be unique');
        }
    }
}
