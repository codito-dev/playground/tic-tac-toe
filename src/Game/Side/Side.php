<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game\Side;

use InvalidArgumentException;

final class Side
{
    /** @var non-empty-array<Player> */
    private array $players;

    public function __construct(private readonly Marker $marker, Player ...$players)
    {
        if (0 === count($players)) {
            throw new InvalidArgumentException('Game side must contain at least one player');
        }

        $this->players = $players;
    }

    public function player(): Player
    {
        return current($this->players);
    }

    public function endMove(): void
    {
        if (false === next($this->players)) {
            reset($this->players);
        }
    }

    public function marker(): Marker
    {
        return $this->marker;
    }
}
