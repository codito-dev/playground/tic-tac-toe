<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game\Side;

final readonly class Player
{
    public function __construct(public string $name) {
        if ('' === $this->name) {
            throw new \InvalidArgumentException('Player name can\'t be empty');
        }
    }
}
