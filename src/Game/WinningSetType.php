<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

enum WinningSetType: string
{
    case Row = 'row';
    case Column = 'column';
    case DiagonalSlope = 'diagonal slope';
    case DiagonalSteep = 'diagonal steep';
}
