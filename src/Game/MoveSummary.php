<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

use Codito\Kata\TicTacToe\Game\Side\Marker;
use Codito\Kata\TicTacToe\Game\Side\Player;

final readonly class MoveSummary
{
    public function __construct(
        public BoardState $boardState,
        public Marker $currentMarker,
        public Player $lastMoveBy,
        public ?WinningSetType $winningSetType = null,
        public ?int $winningSetIndex = null
    ) {}

    public function winningSetSummary(): ?string
    {
        if (null === $this->winningSetType) {
            return null;
        }

        return match($this->winningSetType) {
            WinningSetType::Row, WinningSetType::Column => sprintf(
                '%s %d',
                $this->winningSetType->value,
                $this->winningSetIndex
            ),
            WinningSetType::DiagonalSlope, WinningSetType::DiagonalSteep => $this->winningSetType->value,
        };
    }
}
