<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

use Codito\Kata\TicTacToe\Game\Side\Player;
use JsonSerializable;

/**
 * @phpstan-type SerializedMove array{player: string, row: int, column: int}
 */
final readonly class Move implements JsonSerializable
{
    public function __construct(
        public Player $player,
        public int $row,
        public int $column
    ) {}

    /** @phpstan-param SerializedMove $raw */
    public static function fromArray(array $raw): self
    {
        return new self(new Player($raw['player']), $raw['row'], $raw['column']);
    }

    /** @phpstan-return SerializedMove */
    public function jsonSerialize(): array
    {
        return [
            'player' => $this->player->name,
            'row' => $this->row,
            'column' => $this->column,
        ];
    }
}
