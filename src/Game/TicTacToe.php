<?php

declare(strict_types=1);

namespace Codito\Kata\TicTacToe\Game;

use Codito\Kata\TicTacToe\Game\Side\Marker;
use Codito\Kata\TicTacToe\Game\Side\Sides;
use Codito\Kata\TicTacToe\Game\Side\Player;

final class TicTacToe
{
    private Marker $currentSideMarker;

    public function __construct(private readonly Sides $sides, private readonly Board $board)
    {
        $this->currentSideMarker = Marker::Cross;
    }

    public function playerMove(Move $move): MoveSummary
    {
        $currentPlayer = $this->getCurrentPlayer();

        if ($move->player->name !== $currentPlayer->name) {
            throw new \InvalidArgumentException(sprintf(
                'This is not %s\'s turn',
                $move->player->name
            ));
        }

        $this->board->placeMarker($this->currentSideMarker, $move->row, $move->column);
        $moveSummary = new MoveSummary(
            $this->board->state(),
            $this->currentSideMarker,
            $currentPlayer,
            $this->board->winningSetType(),
            $this->board->winningSetIndex()
        );

        // If board is still open for playing, let's prepare next turn
        if (BoardState::Open === $this->board->state()) {
            $this->sides->side($this->currentSideMarker)->endMove();
            $this->currentSideMarker = $this->currentSideMarker === Marker::Cross ? Marker::Nought : Marker::Cross;
        }

        return $moveSummary;
    }

    private function getCurrentPlayer(): Player
    {
        return $this->sides->side($this->currentSideMarker)->player();
    }
}
