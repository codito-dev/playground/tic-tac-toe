# Modeling kata: Tic-Tac-Toe

This is [Greg Korba](https://twitter.com/_Codito_)'s implementation of Tic-Tac-Toe modeling kata that took place between a bunch of programming enthusiasts. Main driver was knowledge sharing and ability to compare technical approaches.

## Exercise assumptions

- we want to create relatively simple model, which has closed set of rules that can be applied
- focus is on modeling and interactions between models, not on UI (which is optional)
- tests should prove that model is working properly

## Technical assumptions

- model can be prepared in any programming language
- no framework and libraries, excluding testing frameworks

## Personal assumptions

- Refresh Behat experience, because (unfortunately) it is not widely adopted
- Focus on TDD and DDD as much as possible

## Summary

Tic-Tac-Toe was implemented with TDD in mind using Behat. Stories were written first and were polished only a little during development. I did not want to apply full red-green development here with multiple cycles, I just prepared full implementation at once, improving Behat scenarios on-the-fly where it was needed.

### Further improvements

- CLI command that runs the game
- Allowing multiplayer game

### Verification

```shell
docker compose run php vendor/bin/behat
```
