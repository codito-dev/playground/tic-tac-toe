FROM php:8.3.0-cli-bookworm

WORKDIR /app

COPY --from=composer/composer:2 /usr/bin/composer /usr/local/bin

RUN apt-get -q update \
    && apt-get -yq upgrade \
    && apt-get install -yq unzip $PHPIZE_DEPS \
    && pecl -q install \
      xdebug-3.3.0alpha3 \
      pcov \
    && (docker-php-ext-enable xdebug pcov > /dev/null) \
    && apt-get purge -y $PHPIZE_DEPS \
    && apt-get autoremove -y --purge \
    && apt-get clean all \
    && rm -Rf /tmp/* \
