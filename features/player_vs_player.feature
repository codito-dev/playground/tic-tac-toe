Feature: Player versus Player

  Background:
    Given a player with name "Bill"
    And a player with name "Steve"

  Scenario: Player makes a move outside of the grid
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 4,1         |
    Then error occurs with a message "This move is invalid"

  Scenario: Player wants to play the same move as opponent side
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 1,1         |
    Then error occurs with a message "This field is already taken"

  Scenario: Player wants to make multiple moves in a row
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Bill   | 1,2         |
    Then error occurs with a message "This is not Bill's turn"

  Scenario: Players' names are not unique
    Given a player with name "Jeff"
    When game is started
    Then error occurs with a message "Unsupported amount of players"

  Scenario: Steve wins with a row
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 2,2         |
      | Bill   | 3,3         |
      | Steve  | 2,1         |
      | Bill   | 1,3         |
      | Steve  | 2,3         |
    Then player "Steve" wins
    And the winning set is row 2

  Scenario: Bill wins with a column
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 1,2         |
      | Bill   | 2,1         |
      | Steve  | 2,2         |
      | Bill   | 3,1         |
    Then player "Bill" wins
    And the winning set is column 1

  Scenario: Bill wins with a sloping diagonal
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 1,2         |
      | Bill   | 2,2         |
      | Steve  | 3,2         |
      | Bill   | 3,3         |
    Then player "Bill" wins
    And the winning set is diagonal slope

  Scenario: Steve wins with a steeping diagonal
    Given board dimension is 3
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 2,2         |
      | Bill   | 2,1         |
      | Steve  | 3,1         |
      | Bill   | 1,2         |
      | Steve  | 1,3         |
    Then player "Steve" wins
    And the winning set is diagonal steep

  Scenario: Game ends in a draw
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 3,3         |
      | Bill   | 1,2         |
      | Steve  | 1,3         |
      | Bill   | 2,2         |
      | Steve  | 3,2         |
      | Bill   | 3,1         |
      | Steve  | 2,1         |
      | Bill   | 2,3         |
    Then game ends in a draw

  Scenario: Bill wins with a row on bigger board
    Given board dimension is 4
    When game is started
    And players make moves:
      | player | coordinates |
      | Bill   | 1,1         |
      | Steve  | 2,1         |
      | Bill   | 1,2         |
      | Steve  | 2,2         |
      | Bill   | 1,3         |
      | Steve  | 2,3         |
      | Bill   | 1,4         |
    Then player "Bill" wins
    And the winning set is row 1
