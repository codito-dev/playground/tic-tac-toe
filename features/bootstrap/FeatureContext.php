<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Codito\Kata\TicTacToe\Game\Board;
use Codito\Kata\TicTacToe\Game\BoardState;
use Codito\Kata\TicTacToe\Game\Move;
use Codito\Kata\TicTacToe\Game\MoveSummary;
use Codito\Kata\TicTacToe\Game\Side\Player;
use Codito\Kata\TicTacToe\Game\Side\Sides;
use Codito\Kata\TicTacToe\Game\TicTacToe;

class FeatureContext implements Context
{
    /** @var array<Player> */
    private array $players;
    private int $boardSize = 3;
    private TicTacToe $game;
    private ?MoveSummary $moveSummary = null;
    private ?string $errorMessage = null;

    /**
     * @Given a player with name :name
     */
    public function aPlayerWithName(string $name): void
    {
        $this->players[] = new Player($name);
    }

    /**
     * @Given board dimension is :size
     */
    public function boardDimensionIs(int $size): void
    {
        $this->boardSize = $size;
    }

    /**
     * @When game is started
     */
    public function gameIsStarted(): void
    {
        try {
            $this->game = new TicTacToe(
                Sides::createByAlternatingAssignment(...$this->players),
                new Board($this->boardSize)
            );
        } catch (\Throwable $e) {
            $this->errorMessage = $e->getMessage();
        }
    }

    /**
     * @When players make moves:
     *
     * $param iterable<array{player: string, coordinates: string}> $moves
     */
    public function playersMakeMoves(TableNode $moves): void
    {
        try {
            foreach ($moves as $move) {
                [$row, $column] = explode(',', $move['coordinates']);
                $this->moveSummary = $this->game->playerMove(new Move(
                    new Player($move['player']),
                    (int)$row,
                    (int)$column
                ));
            }
        } catch (\Throwable $e) {
            $this->errorMessage = $e->getMessage();
        }
    }

    /**
     * @Then player :name wins
     */
    public function playerWins(string $name): void
    {
        if (BoardState::Win !== $this->moveSummary->boardState || $name !== $this->moveSummary->lastMoveBy->name) {
            throw new LogicException(sprintf(
                'Player %s should have won, but did not',
                $name
            ));
        }
    }

    /**
     * @Then the winning set is :type :detail
     */
    public function theWinningSetIs(string $type, int|string $detail): void
    {
        if ($this->moveSummary->winningSetSummary() !== sprintf('%s %s', $type, $detail)) {
            throw new LogicException(
                sprintf(
                    'Winning set should have been %s %s, but it was not',
                    $type,
                    $detail
                )
            );
        }
    }

    /**
     * @Then game ends in a draw
     */
    public function gameEndsInADraw(): void
    {
        if (BoardState::Draw !== $this->moveSummary->boardState) {
            throw new LogicException('Game should have ended with a draw, but did not');
        }
    }

    /**
     * @Then error occurs with a message :error
     */
    public function errorOccursWithAMessage(string $error): void
    {
        if ($error !== $this->errorMessage) {
            throw new LogicException('Expected error message not matched');
        }
    }
}
